const express = require("express"); //import exports module
const fs = require("fs");
const app = express(); //express first class citizen function is assinge variable

const postData = require("./Posts.json");
// console.log(postData)

//pass incomeing data
app.use(express.json());

//routing
//home route
app.get("/", function (req, res) {
  res.send("Home Route");
});

//#fetch all posts
app.get("/posts", function (req, res) {
  res.json({ message: "Post fetch Successfuly", postData });
});

//Create post
app.post("/posts", function (req, res) {
  //get the post from user
  const newpost = req.body;

  //push the new post into exiting post

  postData.push({ ...newpost, id: postData.length.toString() });
  console.log(postData);
  //write to the file
  fs.writeFile("Posts.json", JSON.stringify(postData), function (err) {
    if (err) {
      console.log(err);
    }
    //send message to the user
    res.json({ message: "Post Created successfully" });
  });
});

//#get single post
app.get("/posts/:id", function (req, res) {
  //get the id the post
  const id = req.params.id;
  // console.log(id)

  //fincd post by id
  const postFound = postData.find((post) => {
    return post.id === id;
  });
  if (!postFound) {
    res.json({ message: "post is not found" });
  } else {
    //send the post to the user
    res.json({ postFound });
  }
});

//#Create Update a put
app.put("/posts/:id", function (req, res) {
  //get the dynamic id === params
  const id = req.params.id;
  const { url, title, desc } = req.body;
  console.log(req.body);

  //find the post to update

  const foundPost = postData.find(function (post) {
    return post.id === id;
  });
  if (!foundPost) return res.json({ msg: "Post is not Found" });

  //filter out all post with the post found
  const filterPostsdata = postData.filter(post=>post.id !==id)
  console.log(filterPostsdata)

  //Update the found post
  foundPost.title=title;
  foundPost.desc=desc;
  foundPost.url=url;

  //push the update post filter post 
  filterPostsdata.unshift(foundPost)

  //write to the file
  fs.writeFile("Posts.json", JSON.stringify(filterPostsdata), function (err) {
    if (err) {
      console.log(err);
    }
    //send message to the user
    res.json({ message: "Post updated successfully" });
  });
});

//Create Delete Req
app.delete("/posts/:id", function (req, res) {
  //get the id
  const id = req.params.id;
  const filterPost = postData.filter(function (post) {
    console.log(post);
    return post.id !== id;
  });
  //write to the file
  fs.writeFile("Posts.json", JSON.stringify(filterPost), function (err) {
    if (err) {
      console.log(err);
    }
    //send message to the user
    res.json({ message: "Post deleted successfully" });
  });
  console.log(filterPost);
});

// Create a server
app.listen(9000, () => {
  console.log("server is up and running 9000");
});
